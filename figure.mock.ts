import * as Yup from 'yup';

const FIGURE_MOCK = {
  _id: Yup.string().required(),
  name: Yup.string().required(),
  primaryColor: Yup.string().required(),
  ageSegment: Yup.string().required(),
  occupation: Yup.array(Yup.string()).required(),
  hobbies: Yup.array(Yup.string()).required(),
  habits: Yup.array(Yup.string()).required(),
  biography: Yup.string().required(),
  productWorld: Yup.string().required(),
  __v: Yup.number().required(),
  createdAt: Yup.date().required(),
  updatedAt: Yup.date().required(),
  videos: Yup.array(
    Yup.object({
      link: Yup.string(),
      title: Yup.string(),
      description: Yup.string()
    })
  ),
  uploadedVideos: Yup.array(
    Yup.object({
      fileId: Yup.string(),
      fileUrl: Yup.string(),
      name: Yup.string(),
      description: Yup.string(),
      thumbnail: Yup.string()
    })
  ),
  slug: Yup.string().required(),
  mapLocation: Yup.object({
    x: Yup.string(),
    y: Yup.string()
  }).required()
};

const getFiguresOkay = {
  prepareFunction: () => ({}),
  validObject: Yup.array(Yup.object({ ...FIGURE_MOCK }))
};

const CreateFigureOkay = {
  prepareFunction: (productWorld: string): ICreateFigureDTO => ({
    name: `figura Slon`,
    primaryColor: `plava boja`,
    ageSegment: 'adult',
    occupation: ['janitor'],
    hobbies: ['Handicrafts, Ships in bottles'],
    habits: ['Always has a hamburger with him'],
    biography:
      'Tony is a good-hearted, clumsy elephant. He is active as a craftsman in Animal Buddies and is called in almost all craft tasks.',
    productWorld,
    videos: [
      {
        link: 'https://www.youtube.com/watch?v=G1IbRujko-A',
        title: 'Slon title',
        description: 'Slon description'
      }
    ],
    uploadedVideos: [],
    slug: 'SLON',
    mapLocation: { x: 13.12, y: 22.59 }
  })
};

export { FIGURE_MOCK, CreateFigureOkay, getFiguresOkay };
