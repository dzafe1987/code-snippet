import { Request, Response } from 'express';
import { FigureService } from './figure.service';
import { Helper } from '../modules/helper.module';
import OneSignalModule from '../modules/onesignal.module';
import { NofiticationType } from '../constants/enums';
import { ProductService } from '../product/product.service';

const create = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.create(req.body);
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const sendNotification = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.sendNotificationFigure(
      req.params.figureId
    );
    OneSignalModule.createOneSignalNotification(NofiticationType.NEW_FIGURE, {
      figure
    });
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const uploadPicture = async (req: Azure.Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.uploadPictureToFigure(req);
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const uploadMobilePicture = async (req: Azure.Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.uploadMobilePictureToFigure(req);
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const uploadVideo = async (req: Azure.Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.uploadVideoToFigure(req);
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const putModel = async (req: Azure.Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.addModelToAFigure(req);
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const putTexture = async (req: Azure.Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.addTextureToAFigure(req);
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const deletePicture = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.deletePictureToFigure(
      req.params.figureId
    );
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const deleteMobilePicture = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.deleteMobilePictureToFigure(
      req.params.figureId
    );
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const deleteVideo = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.deleteVideoToFigure(
      req.params.figureId,
      req.body.fileId
    );
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const getFigures = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figures = await figureService.getFigures(req.query);
    return Helper.generateResponse(res, figures);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const findById = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.findOne({ _id: req.params.id }, [
      {
        path: 'image'
      },
      {
        path: 'mobile_image'
      },
      {
        path: 'figureModel'
      },
      {
        path: 'figureTexture'
      },
      {
        path: 'product'
      }
    ]);
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const deleteModel = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.deleteModelFromAFigure(
      req.params.figureId
    );
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const deleteTexture = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.deleteTextureFromAFigure(
      req.params.figureId
    );
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const latestFigure = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.latestFigure();
    return Helper.generateResponse(res, figure);
  } catch (error) {
    return Helper.generateFailure(res, error);
  }
};

const update = async (req: Request, res: Response) => {
  try {
    const figureService = new FigureService();
    const figure = await figureService.update(
      { _id: req.params.id },
      req.body,
      true
    );
    return Helper.generateResponse(res, figure);
  } catch (error) {
    console.log('errrrr', error);
    return Helper.generateFailure(res, error);
  }
};

export {
  create,
  uploadPicture,
  deletePicture,
  putModel,
  sendNotification,
  deleteModel,
  getFigures,
  findById,
  latestFigure,
  putTexture,
  deleteTexture,
  update,
  uploadVideo,
  deleteVideo,
  uploadMobilePicture,
  deleteMobilePicture
};
