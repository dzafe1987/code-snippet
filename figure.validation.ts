import lodashPick from 'lodash/pick';
import * as Yup from 'yup';
import { StandardValidation } from '../modules/standard-validation.module';
import { Helper } from '../modules/helper.module';
import s from '../constants/strings';

class FigureValidationClass extends StandardValidation {
  figureValidateObject = {
    name: Yup.string().required(),
    primaryColor: Yup.string().required(),
    ageSegment: Yup.string().required(),
    occupation: Yup.array(Yup.string()).required(),
    hobbies: Yup.array(Yup.string()).required(),
    habits: Yup.array(Yup.string()).required(),
    biography: Yup.string().required(),
    productWorld: Yup.string().required(),
    videos: Yup.array(
      Yup.object({
        link: Yup.string(),
        title: Yup.string(),
        description: Yup.string()
      })
    ),
    slug: Yup.string().required(),
    mapLocation: Yup.object({
      x: Yup.string(),
      y: Yup.string()
    }).required()
  };

  create = async (createFigureDTO: ICreateFigureDTO) => {
    const validateObject = Yup.object(
      lodashPick(this.figureValidateObject, [
        'name',
        'primaryColor',
        'ageSegment',
        'occupation',
        'hobbies',
        'habits',
        'biography',
        'productWorld',
        'videos',
        'slug',
        'mapLocation'
      ])
    );
    try {
      await validateObject.validate(createFigureDTO, { abortEarly: false });
    } catch (e) {
      Helper.handleYupError(e, s.FIGURE.cannotCreateFigure.value);
    }
  };
}

export const FigureValidation = new FigureValidationClass();
