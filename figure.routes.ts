import { Router } from 'express';
import * as FigureController from './figure.controller';
import JWTModule from '../modules/jwt.module';
import Azure from '../modules/azure.module';
import { FileType } from '../constants/enums';

const router = Router();

router.route('/latest-figure').get(FigureController.latestFigure);

router
  .route('/:figureId/image')
  .put(
    JWTModule.authJWT,
    JWTModule.isAdmin,
    Azure.handlePreUpload(FileType.DEFAULT),
    FigureController.uploadPicture
  )
  .delete(JWTModule.authJWT, JWTModule.isAdmin, FigureController.deletePicture);

router
  .route('/:figureId/mobile-image')
  .put(
    JWTModule.authJWT,
    JWTModule.isAdmin,
    Azure.handlePreUpload(FileType.DEFAULT),
    FigureController.uploadMobilePicture
  )
  .delete(
    JWTModule.authJWT,
    JWTModule.isAdmin,
    FigureController.deleteMobilePicture
  );

router
  .route('/:figureId/model')
  .put(
    JWTModule.authJWT,
    JWTModule.isAdmin,
    Azure.handlePreUpload(FileType.MODEL),
    FigureController.putModel
  )
  .delete(JWTModule.authJWT, JWTModule.isAdmin, FigureController.deleteModel);

router
  .route('/:figureId/texture')
  .put(
    JWTModule.authJWT,
    JWTModule.isAdmin,
    Azure.handlePreUpload(FileType.DEFAULT),
    FigureController.putTexture
  )
  .delete(JWTModule.authJWT, JWTModule.isAdmin, FigureController.deleteTexture);

router
  .route('/:figureId/notification')
  .post(
    JWTModule.authJWT,
    JWTModule.isAdmin,
    FigureController.sendNotification
  );

router
  .route('/:figureId/video')
  .put(
    JWTModule.authJWT,
    JWTModule.isAdmin,
    Azure.handlePreUpload(FileType.DEFAULT),
    FigureController.uploadVideo
  )
  .delete(JWTModule.authJWT, JWTModule.isAdmin, FigureController.deleteVideo);

router
  .route('/')
  .get(FigureController.getFigures)
  .post(JWTModule.authJWT, JWTModule.isAdmin, FigureController.create);
router
  .route('/:id')
  .get(JWTModule.authJWT, FigureController.findById)
  .put(JWTModule.authJWT, JWTModule.isAdmin, FigureController.update);

export default router;
