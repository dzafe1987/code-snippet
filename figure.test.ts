import { CreateFigureOkay, FIGURE_MOCK } from './figure.mock';
import { FigureService } from './figure.service';
import { ProductWorldService } from '../product-world/productWorld.service';
import { CreateProductWorldOkay } from '../product-world/productWorld.mock';
import { mongooseConnect, checkYupValidations, getUserToken } from '../tests';
import supertest from 'supertest';
import { server } from '../tests';
import SwaggerModule from '../modules/swagger.module';
import { ADMIN_CREDENTIALS } from '../constants';
import { UserService } from '../user/user.service';
import { CreateWithConfirmationTokenOkay } from '../user/user.mock';

const request = supertest(server);

let PreparedProductWorld = null;
let PreparedUser = null;
let PreparedUserToken = null;
let FigureToCreate = null;
let ADMIN_TOKEN = null;
let createdFigure = null;
let FigureCreated = null;

beforeAll(done => {
  mongooseConnect(async () => {
    const productWorldService = new ProductWorldService();
    const productWorldToCreate = CreateProductWorldOkay.prepareFunction();
    PreparedProductWorld = await productWorldService.create(
      productWorldToCreate
    );
    const userService = new UserService();
    const userToCreate = CreateWithConfirmationTokenOkay.prepareFunction();
    PreparedUser = await userService.create(userToCreate);
    await userService.afterConfirmationActions(PreparedUser._id);
    PreparedUser = await userService.findOne({ _id: PreparedUser._id });

    FigureToCreate = CreateFigureOkay.prepareFunction(PreparedProductWorld._id);
    PreparedUserToken = await getUserToken(userToCreate);

    ADMIN_TOKEN = await getUserToken(ADMIN_CREDENTIALS);

    const figureService = new FigureService();
    const newFigureToCreate = { ...FigureToCreate, name: 'testForModel' };
    FigureCreated = await figureService.create(newFigureToCreate);

    done();
  });
});

describe('Figure service - createFigure', () => {
  const figureService = new FigureService();

  it('should create figure ', async () => {
    createdFigure = await figureService.create(FigureToCreate);

    const isValid = await checkYupValidations(createdFigure, FIGURE_MOCK);
    expect(isValid).toBe(true);
  });

  it('should not create figure with no name', async () => {
    const newFigureToCreate = { ...FigureToCreate, name: '' };
    try {
      await figureService.create(newFigureToCreate);
      expect(false).toBe(true);
    } catch (e) {
      expect(true).toBe(true);
    }
  });

  it('should not create figure without productWorld(productWorld ID)', async () => {
    const newFigureToCreate = { ...FigureToCreate, productWorld: '' };
    try {
      await figureService.create(newFigureToCreate);
      expect(false).toBe(true);
    } catch (e) {
      expect(true).toBe(true);
    }
  });

  it('should not create figure - figure exists', async () => {
    try {
      await figureService.create(FigureToCreate);
      expect(false).toBe(true);
    } catch (e) {
      expect(true).toBe(true);
    }
  });

  it('should fetch a single figure', async () => {
    const figureId = FigureCreated._id;
    try {
      await figureService.findOne(`/api/v1/figures/${figureId}`);
      expect(false).toBe(true);
    } catch (e) {
      expect(true).toBe(true);
    }
  });

  it('should upload picture to figure and get latest', async () => {
    try {
      const pathToFile = `${__dirname}/test_file.jpg`;
      const realPath = pathToFile.replace('/figure/', '/tests/');

      const responseWhenPost = await request
        .put(`/api/v1/figures/${createdFigure._id}/image`)
        .set('Authorization', ADMIN_TOKEN)
        .attach('file', realPath);

      const theLatestFigure = await figureService.latestFigure();

      const responseWhenDelete = await request
        .delete(`/api/v1/figures/${createdFigure._id}/image`)
        .set('Authorization', ADMIN_TOKEN);

      expect(responseWhenDelete.status).toBe(200);

      expect(responseWhenPost.status).toBe(200);
      expect(theLatestFigure._id.toString()).toBe(createdFigure._id.toString());
    } catch (e) {
      console.log('Test error', e);
    }
  });
});

describe('Initial integration test with swagger', () => {
  it('Figure /figure create', async done => {
    const body = FigureToCreate;
    const swaggerThings = SwaggerModule.generateUrlSwagger(
      'post',
      'figure',
      'Create /figure - only admin',
      '/api/v1/figures',
      {
        path: [],
        query: []
      },
      'application/json',
      true,
      body
    );
    const response = await request
      .post(swaggerThings.url)
      .set('Authorization', ADMIN_TOKEN)
      .send(body);
    expect(response.status).toBe(200);

    swaggerThings.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerThings.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerThings.swaggerPreparation, done);
  });

  it('Figure /Figures get all', async done => {
    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'get',
      'figure',
      'Get /figures',
      '/api/v1/figures',
      {
        path: [],
        query: [
          {
            name: 'productWorldId',
            description:
              'Filter figures by product world id (Array is supported)',
            value: PreparedProductWorld._id
          }
        ]
      },
      'application/json',
      false
    );

    const response = await request.get(swaggerValues.url);

    expect(response.status).toBe(200);
    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );
    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });
});

describe('Integration test - Get Figure by Id', () => {
  it('Figure /Figures get one by id', async done => {
    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'get',
      'figure',
      'Get figure by id',
      '/api/v1/figures/:figureId',
      {
        path: [
          {
            name: 'figureId',
            value: FigureCreated._id,
            required: true
          }
        ],
        query: []
      },
      'application/json',
      true
    );

    const response = await request
      .get(swaggerValues.url)
      .set('Authorization', `${PreparedUserToken}`);
    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });
});

describe('Integration test for user figure model', () => {
  it('Should update model of figure', async done => {
    const body = {
      files: [
        {
          name: 'file',
          isArray: false,
          required: true
        }
      ]
    };

    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'put',
      'figure',
      'Update model of a figure',
      '/api/v1/figures/:figureId/model',
      {
        path: [
          {
            name: 'figureId',
            value: FigureCreated._id,
            required: true
          }
        ],
        query: []
      },
      'multipart/form-data',
      true,
      body
    );

    const pathToFile = `${__dirname}/test_model.model`;
    const realPath = pathToFile.replace('/figure/', '/tests/');
    const response = await request
      .put(swaggerValues.url)
      .set('Authorization', ADMIN_TOKEN)
      .attach('file', realPath);
    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });

  it('Should delete model of figure', async done => {
    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'delete',
      'figure',
      'Delete model of a figure',
      '/api/v1/figures/:figureId/model',
      {
        path: [
          {
            name: 'figureId',
            value: FigureCreated._id,
            required: true
          }
        ],
        query: []
      },
      'multipart/form-data', //'application/json',
      true
    );

    const response = await request
      .delete(swaggerValues.url)
      .set('Authorization', ADMIN_TOKEN);

    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });

  it('Should update texture of figure', async done => {
    const body = {
      files: [
        {
          name: 'file',
          isArray: false,
          required: true
        }
      ]
    };

    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'put',
      'figure',
      'Update texture of a figure',
      '/api/v1/figures/:figureId/texture',
      {
        path: [
          {
            name: 'figureId',
            value: FigureCreated._id,
            required: true
          }
        ],
        query: []
      },
      'multipart/form-data',
      true,
      body
    );

    const pathToFile = `${__dirname}/test_file.png`;
    const realPath = pathToFile.replace('/figure/', '/tests/');
    const response = await request
      .put(swaggerValues.url)
      .set('Authorization', ADMIN_TOKEN)
      .attach('file', realPath);
    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });

  it('Should delete model of figure', async done => {
    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'delete',
      'figure',
      'Delete model of a figure',
      '/api/v1/figures/:figureId/texture',
      {
        path: [
          {
            name: 'figureId',
            value: FigureCreated._id,
            required: true
          }
        ],
        query: []
      },
      'multipart/form-data', //'application/json',
      true
    );

    const response = await request
      .delete(swaggerValues.url)
      .set('Authorization', ADMIN_TOKEN);

    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });
});

describe('Integration test for figure picture', () => {
  it('Should update picture of figure', async done => {
    const body = {
      files: [
        {
          name: 'file',
          isArray: false,
          required: true
        }
      ]
    };

    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'put',
      'figure',
      'Update figure image',
      '/api/v1/figures/{figureId}/image',
      {
        path: [
          {
            name: 'figureId',
            value: createdFigure._id,
            required: true
          }
        ],
        query: []
      },
      'multipart/form-data',
      true,
      body
    );

    const pathToFile = `${__dirname}/test_file.jpg`;
    const realPath = pathToFile.replace('/figure/', '/tests/');
    const response = await request
      .put(swaggerValues.url)
      .set('Authorization', ADMIN_TOKEN)
      .attach('file', realPath);
    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });
});

describe('Integration test for figure picture delete', () => {
  it('Should delete picture of figure', async done => {
    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'delete',
      'figure',
      'Delete figure image',
      '/api/v1/figures/{figureId}/image',
      {
        path: [
          {
            name: 'figureId',
            value: createdFigure._id,
            required: true
          }
        ],
        query: []
      },
      'multipart/form-data', //'application/json',
      true
    );

    const response = await request
      .delete(swaggerValues.url)
      .set('Authorization', ADMIN_TOKEN);

    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });
});

describe('Integration test for figure video', () => {
  it('Should update picture of figure', async done => {
    const body = {
      files: [
        {
          name: 'file',
          isArray: false,
          required: true
        }
      ],
      name: 'Video Name',
      description: 'Video description',
      thumbnail: 'v1de0thumbn41l_ID'
    };

    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'put',
      'figure',
      'Update figure video',
      '/api/v1/figures/{figureId}/video',
      {
        path: [
          {
            name: 'figureId',
            value: createdFigure._id,
            required: true
          }
        ],
        query: []
      },
      'multipart/form-data',
      true,
      body
    );

    const pathToFile = `${__dirname}/test_file.jpg`;
    const realPath = pathToFile.replace('/figure/', '/tests/');
    const response = await request
      .put(swaggerValues.url)
      .set('Authorization', ADMIN_TOKEN)
      .attach('file', realPath);
    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });
});

describe('Integration test for figure video delete', () => {
  it('Should delete picture of figure', async done => {
    const figureService = new FigureService();
    const temporary = await figureService.findOne({ _id: createdFigure._id });
    const body = {
      fileId: temporary.uploadedVideos[0].fileId
    };
    console.log({ bodtTest: body });
    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'delete',
      'figure',
      'Delete figure video',
      '/api/v1/figures/{figureId}/video',
      {
        path: [
          {
            name: 'figureId',
            value: createdFigure._id,
            required: true
          }
        ],
        query: []
      },
      'application/json',
      true,
      body
    );

    const response = await request
      .delete(swaggerValues.url)
      .send(body)
      .set('Authorization', ADMIN_TOKEN);

    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
  });
});

describe('Integration test for latest figure', () => {
  const figureService = new FigureService();
  it('Should get latest figure', async done => {
    const pathToFile = `${__dirname}/test_file.jpg`;
    const realPath = pathToFile.replace('/figure/', '/tests/');

    const responseWhenPost = await request
      .put(`/api/v1/figures/${createdFigure._id}/image`)
      .set('Authorization', ADMIN_TOKEN)
      .attach('file', realPath);

    const swaggerValues = SwaggerModule.generateUrlSwagger(
      'get',
      'figure',
      'Get latest figure with populated image',
      '/api/v1/figures/latest-figure',
      {
        path: [],
        query: []
      },
      'application/json',
      true
    );

    const response = await request.get(swaggerValues.url);

    expect(response.status).toBe(200);

    swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
      response.status,
      response.body,
      swaggerValues.swaggerPreparation
    );

    SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);

    const responseWhenDelete = await request
      .delete(`/api/v1/figures/${createdFigure._id}/image`)
      .set('Authorization', ADMIN_TOKEN);
  });

  describe('Figure Service - Update figure', () => {
    it('Should update figure', async done => {
      const swaggerValues = SwaggerModule.generateUrlSwagger(
        'put',
        'figure',
        'This is for updating figure',
        '/api/v1/figures/{id}',
        {
          path: [
            {
              name: 'id',
              value: createdFigure._id,
              description: 'Id of figure thats been updating',
              required: true
            }
          ],
          query: []
        },
        'application/json',
        false,
        [
          {
            name: 'name',
            value: 'New name',
            description: 'Update figure name',
            required: false
          },
          {
            name: 'primaryColor',
            value: 'wow',
            description: 'Update figure primaryColor',
            required: false
          },
          {
            name: 'ageSegment',
            value: 'blue',
            description: 'Update figure ageSegment',
            required: false
          },
          {
            name: 'occupation',
            value: ['test'],
            description: 'Update figure occupation',
            required: false
          },
          {
            name: 'hobbies',
            value: ['test'],
            description: 'Update figure hobbies',
            required: false
          },
          {
            name: 'habits',
            value: ['test'],
            description: 'Update figure habits',
            required: false
          },
          {
            name: 'videos',
            value: [],
            description: 'Update figure videos',
            required: false
          },
          {
            name: 'biography',
            value: 'blue',
            description: 'Update figure color',
            required: false
          },
          {
            name: 'slug',
            value: 'blue',
            description: 'Update figure slug',
            required: false
          },
          {
            name: 'mapLocation',
            value: { x: 1, y: 1 },
            description: 'Update figure mapLocation',
            required: false
          }
        ]
      );

      const response = await request
        .put(swaggerValues.url)
        .set('Authorization', ADMIN_TOKEN);

      expect(response.status).toBe(200);

      swaggerValues.swaggerPreparation = SwaggerModule.updateResponses(
        response.status,
        response.body,
        swaggerValues.swaggerPreparation
      );

      SwaggerModule.writeToSwagger(swaggerValues.swaggerPreparation, done);
    });
  });
});
