import mongoose, { Schema, Model } from 'mongoose';

const FigureSchema = new Schema<IFigure>(
  {
    name: {
      type: String
    },
    primaryColor: {
      type: String
    },
    ageSegment: {
      type: String
    },
    occupation: [
      {
        type: String
      }
    ],
    hobbies: [
      {
        type: String
      }
    ],
    habits: [
      {
        type: String
      }
    ],
    videos: [
      {
        link: { type: String },
        title: { type: String },
        description: { type: String }
      }
    ],
    uploadedVideos: [
      {
        fileId: { type: String },
        fileUrl: { type: String },
        thumbnail: {
          type: String,
          required: false
        },
        description: {
          type: String,
          required: false
        },
        name: {
          type: String,
          required: false
        }
      }
    ],
    biography: {
      type: String
    },
    productWorld: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'ProductWorld'
    },
    image: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'File'
    },
    mobile_image: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'File'
    },
    figureModel: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'File'
    },
    figureTexture: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'File'
    },
    product: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product'
    },
    slug: {
      type: String
    },
    mapLocation: {
      x: {
        type: Number
      },
      y: {
        type: Number
      }
    }
  },
  { timestamps: true }
);

FigureSchema.index({ productWorld: 1 });

const Figure: Model<IFigure> = mongoose.model<IFigure, Model<IFigure>>(
  'Figure',
  FigureSchema
);

export default Figure;
