import FigureModel from './figure.entity';
import { createStandardService } from '../modules/standard-service.module';
import { FigureValidation } from './figure.validation';
import { FileService } from '../file/file.service';
import { Helper } from '../modules/helper.module';

export class FigureService extends createStandardService(
  FigureModel,
  FigureValidation
) {
  uploadPictureToFigure = async (req: Azure.Request) => {
    const fileService = new FileService();
    const fileUploaded = await fileService.upload(req.file, 'figure');
    const figure = await this.findOne({ _id: req.params.figureId });
    if (figure.image) {
      await fileService.delete(figure.image);
    }
    return await this.update(
      { _id: figure._id },
      { image: fileUploaded },
      true
    );
  };

  uploadMobilePictureToFigure = async (req: Azure.Request) => {
    const fileService = new FileService();
    const fileUploaded = await fileService.upload(req.file, 'figure');
    const figure = await this.findOne({ _id: req.params.figureId });
    if (figure.mobile_image) {
      await fileService.delete(figure.mobile_image);
    }
    return await this.update(
      { _id: figure._id },
      { mobile_image: fileUploaded },
      true
    );
  };

  uploadVideoToFigure = async (req: Azure.Request) => {
    const fileService = new FileService();
    const fileUploaded = await fileService.upload(req.file, 'figure');

    const figure = await this.findOne({ _id: req.params.figureId });
    const videos = [...figure.uploadedVideos];
    videos.push({
      fileId: fileUploaded._id,
      fileUrl: fileUploaded.url,
      thumbnail: req.body.thumbnail,
      description: req.body.description,
      name: req.body.name
    });
    return await this.update(
      { _id: figure._id },
      { uploadedVideos: videos },
      true
    );
    return null;
  };

  addModelToAFigure = async (req: Azure.Request) => {
    const fileService = new FileService();
    const fileUploaded = await fileService.upload(req.file, '3Dmodel');
    const figure = await this.findOne({ _id: req.params.figureId });
    if (figure.figureModel) {
      await fileService.delete(figure.figureModel as string);
    }
    return await this.update(
      { _id: figure._id },
      { figureModel: fileUploaded },
      true
    );
  };

  addTextureToAFigure = async (req: Azure.Request) => {
    const fileService = new FileService();
    const fileUploaded = await fileService.upload(req.file, 'texture');
    const figure = await this.findOne({ _id: req.params.figureId });
    if (figure.figureTexture) {
      await fileService.delete(figure.figureTexture as string);
    }
    return await this.update(
      { _id: figure._id },
      { figureTexture: fileUploaded },
      true
    );
  };

  sendNotificationFigure = async (figureId: string) => {
    const figure = await this.findOne({ _id: figureId });
    return figure;
  };

  deleteModelFromAFigure = async (id: string) => {
    const fileService = new FileService();
    const figure = await this.findOne({ _id: id });
    await fileService.delete(figure.figureModel as string);
    return await this.update({ _id: id }, { figureModel: null }, true);
  };

  deleteTextureFromAFigure = async (id: string) => {
    const fileService = new FileService();
    const figure = await this.findOne({ _id: id });
    await fileService.delete(figure.figureTexture as string);
    return await this.update({ _id: id }, { figureTexture: null }, true);
  };

  deletePictureToFigure = async (figureId: string) => {
    const figure = await this.findOne({ _id: figureId });
    if (figure.image) {
      const fileService = new FileService();
      await fileService.delete(figure.image);
    }
    return await this.update({ _id: figureId }, { image: null }, true);
  };

  deleteMobilePictureToFigure = async (figureId: string) => {
    const figure = await this.findOne({ _id: figureId });
    if (figure.mobile_image) {
      const fileService = new FileService();
      await fileService.delete(figure.mobile_image);
    }
    return await this.update({ _id: figureId }, { mobile_image: null }, true);
  };

  deleteVideoToFigure = async (figureId: string, fileId: string) => {
    const figure = await this.findOne({ _id: figureId });
    const videos = [];
    let shouldDelete = false;
    figure.uploadedVideos.forEach(element => {
      if (element.fileId.toString() == fileId.toString()) {
        shouldDelete = true;
      } else {
        videos.push(element);
      }
    });
    //Delete a file
    if (shouldDelete) {
      const fileService = new FileService();
      await fileService.delete(fileId);
    }
    return await this.update(
      { _id: figureId },
      { uploadedVideos: videos },
      true
    );
  };

  getFigures = async (query: { productWorldId?: string | string[] }) => {
    const figures = await this.findAll(
      {
        ...Helper.generateStandardConditionQuery({
          productWorld: query.productWorldId
        })
      },
      [
        {
          path: 'image'
        },
        {
          path: 'mobile_image'
        },
        {
          path: 'figureTexture'
        },
        {
          path: 'figureModel'
        },
        {
          path: 'product'
        }
      ]
    );

    return figures;
  };

  latestFigure = async () => {
    const figures = await this.findAll(
      {
        image: { $ne: null }
      },
      [
        { path: 'image' },
        { path: 'mobile_image' },
        { path: 'product', populate: [{ path: 'gallery' }] }
      ],
      0,
      200,
      {
        createdAt: -1
      }
    );
    const latestFigureItem = figures.find(
      element => element.product.isDeleted === false
    );
    return latestFigureItem;
    // return this.findOne(
    //   {
    //     image: { $ne: null }
    //   },
    //   [{ path: 'image' }, { path: 'product', populate: [{ path: 'gallery' }] }],
    //   false,
    //   {
    //     createdAt: -1
    //   }
    // );
  };
}
