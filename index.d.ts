declare interface IFigure extends Mongoose.Document {
  figureModel: string | IFile;
  figureTexture: string | IFile;
  name: string;
  primaryColor: string;
  ageSegment: string;
  occupation: string[];
  hobbies: string[];
  habits: string[];
  biography: string;
  productWorld: string | IProductWorld;
  image: string | IFile;
  product: string | IProduct;
  videos: IVideo[];
  slug: string;
  mapLocation: IMapLocation;
}

declare interface ICreateFigureDTO {
  name: string;
  primaryColor: string;
  ageSegment: string;
  occupation: string[];
  hobbies: string[];
  habits: string[];
  biography: string;
  productWorld: string | IProductWorld;
  videos: IVideo[];
  uploadedVideos: IUploadedVideo[];
  slug: string;
  mapLocation: IMapLocation;
}

declare interface IVideo {
  link: string;
  title: string;
  description: string;
}

declare interface IUploadedVideo {
  fileId: string;
  fileUrl: string;
  thumbnail?: string;
  description?: string;
  name?: string;
}

declare interface IMapLocation {
  x: number;
  y: number;
}
